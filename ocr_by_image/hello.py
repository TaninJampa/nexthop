import pytesseract as tess
from PIL import Image

tess.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

image_file = 'thai.png'

print(tess.image_to_string(Image.open(image_file), lang='tha').replace(' ', ''))
