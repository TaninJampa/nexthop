import pytesseract as tess
from PIL import Image

tess.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

image = Image.open('thai.png')
text = tess.image_to_string(image,lang='tha')

with open('textfile2.txt','w', encoding="utf-8") as file:
    file.write(text)

print(text)