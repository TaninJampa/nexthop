/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env:{
    mongodburl:"mongodb://localhost:27017"
  }
}

module.exports = nextConfig
