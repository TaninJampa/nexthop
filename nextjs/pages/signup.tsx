export default function Signup() {
    return (
        <div className="min-h-full flex items-center justify-center my-32">
            <div className="max-w-md w-full space-y-8">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Sign Up</h2>
                    <p className="mt-2 text-center text-sm text-gray-600">
                        Or
                        <a href="#" className="font-medium text-indigo-600 hover:text-indigo-500 px-2">Sign in </a>
                    </p>
                </div>
                <form className="mt-8 space-y-6">
                    <div className="rounded-md shadow-sm -space-y-px">
                        <div>
                            <input type="text" autoComplete="none" required className="appearance-none py-2 rounded-none relative block w-full
                            px-3 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md mb-2 focus:outline-none focus:ring-indigo-500 
                            focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="Name" />
                        </div>

                        <div>
                            <input type="email" autoComplete="none" required className="appearance-none py-2 rounded-none relative block w-full
                            px-3 border border-gray-300 placeholder-gray-500 text-gray-900 mb-2 focus:outline-none focus:ring-indigo-500
                            focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="Email Address" />
                        </div>

                        <div>
                            <input type="password" autoComplete="none" required className="appearance-none py-2 rounded-none relative block w-full
                            px-3 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500
                            focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="Password" />
                        </div>
                    </div>

                    <div>
                        <button className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-md font-medium rounded-md
                         text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Sign Up
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}