export default function Contact() {
    return (
        <div className="bg-gray-100 m-0 p-0">
            <div className="flex justify-center place-items-center flex-col ">
                <h1 className="text-center text-gray-800 text-6xl font-bold mt-8 mb-8">Contact Us</h1>

                <div className="bg-gradient-to-br from-indigo-700 to-pink-400 w-2/5 p-6 rounded-lg shadow-lg">
                    <form className="mx-auto">
                        <div className="my-3 mx-auto">
                            <input type="text" className="w-full mt-2 p-4 outline-none rounded-lg" placeholder="Enter Your Name" />
                        </div>

                        <div className="my-3 mx-auto">
                            <input type="email" className="w-full mt-2 p-4 outline-none rounded-lg" placeholder="Enter Your Email" />
                        </div>

                        <div className="my-3 mx-auto">
                            <input type="password" className="w-full mt-2 p-4 outline-none rounded-lg" placeholder="Enter Your Password" />
                        </div>

                        <div className="my-3 mx-auto">
                            <textarea className="w-full h-36 mt-2 p-4 outline-none border-none rounded-lg" placeholder="Type Message..."></textarea>
                        </div>

                        <button className="w-full p-3 mt-2 bg-indigo-700 text-lg text-white rounded-lg outline-none border-none font-bold tracking-wide transition-all hover:bg-indigo-900" >
                            Submit
                        </button>
                    </form>
                </div>
                <br/>
                <br/>
            </div>
        </div>
    )
}