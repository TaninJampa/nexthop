import Link from "next/link"

export default function AboutPage() {
  return (
    <div className='text-3xl font-bold underline text-center text-lime-500'>
      <h1>About</h1>

      <Link href="/" className='text-3xl font-bold underline text-center text-lime-500'>
        Back home
      </Link>
    </div>
  )
}