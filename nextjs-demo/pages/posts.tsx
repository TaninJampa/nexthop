export default function Posts({ posts }: { posts: any }) {
    return (
        <div className='text-3xl font-bold underline text-center text-lime-500 justify-center'>
            <h2>Posts</h2>
            <ul>
                {posts.map((post:any) => (
                    <li key={post.id}>{post.title}</li>
                ))}
            </ul>
        </div>
    )
}

export async function  getStaticProps() {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts')
    const posts = await res.json()

    return {
        props: {
            posts,
        },
    }
}