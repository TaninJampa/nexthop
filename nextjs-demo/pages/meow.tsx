import Image from "next/image";

export default function Meow({ meow }: { meow: any }) {
    console.log(meow);
    
    return (
        <div className='text-3xl font-bold underline text-center text-lime-500 justify-center'>
            <h2>Meow</h2>
            <Image src={meow?.url} width="300" height="300" alt={meow?.file} />
        </div>
    )
}

export async function  getStaticProps() {
    const res = await fetch('https://meow.senither.com/v1/random')
    const response = await res.json()
    console.log(res);
    
    return {
        props: {
            meow: response.data,
        },
        revalidate: 5,
    }
}