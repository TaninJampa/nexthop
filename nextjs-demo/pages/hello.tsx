export default function Hello({title}:{title:any}) {
    return (
        <p>{title}</p>
    )
}

export async function getServerSideProps({context}:{context:any}) {
    return{
        props: {title: 'This is title from getServerSideProps'},
    }
}