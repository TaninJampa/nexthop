import React from "react";
import { ITask } from "./Interfaces";

interface Props {
    task: ITask;
    completeTask(taskNameToDelete: string): void;
}

const TodoTask = ({ task, completeTask }: Props) => {
    return (
        <div>
            <div className="flex flex-row">
                <span className="flex flex-1 p-2 border-solid border-2 border-indigo-600 bg-gray-50 rounded-md">{task.taskName}</span>
                <span className="flex flex-1 p-2 border-solid border-2 border-indigo-600  bg-gray-50 rounded-md">{task.deadline}</span>
                <button className="p-2 border-solid border-2 border-red-800 bg-red-600 rounded-full text-gray-100"
                    onClick={() => {
                        completeTask(task.taskName);
                    }}
                >
                    X
                </button>
            </div>

        </div>
    );
};

export default TodoTask;