import { useState, useEffect, ChangeEvent } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from '@mui/material';
import TodoTask from "./TodoTask";
import { ITask } from "./Interfaces";

function App() {
  const [id, setId] = useState("1");
  const [data, setData]: any[] = useState([]);

  
  const [err, setErr] = useState(false);
  const [task, setTask] = useState<string>("");
  const [deadline, setDealine] = useState<number>(0);
  const [todoList, setTodoList] = useState<ITask[]>([]);

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    if (event.target.name === "task") {
      setTask(event.target.value);
    } else {
      setDealine(Number(event.target.value));
    }
  };

  const addTask = (): void => {
    const newTask = { taskName: task, deadline: deadline };
    setTodoList([...todoList, newTask]);
    setTask("");
    setDealine(0);
  };

  const completeTask = (taskNameToDelete: string): void => {
    setTodoList(
      todoList.filter((task) => {
        return task.taskName !== taskNameToDelete;
      })
    );
  };

  async function getPokemon() {
    try {
      let res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
      let pokemonData = await res.json();
      setData(pokemonData);
    } catch (err) {
      setData(false);
      setErr(true);
    }
  }

  useEffect(() => {
    getPokemon();
    console.log(data);
  }, [])

  console.log(id);

  function handleSubmit(e: any) {
    e.preventDefault();
    getPokemon();
  }

  return (
    <div className="flex items-center justify-center h-screen bg-gradient-to-br from-indigo-700 to-pink-400">
      <span className="text-4xl box-decoration-slice bg-gradient-to-r from-indigo-600 to-pink-500 text-white px-2 ...">
        Pokemon<br />
        Lists
      </span>
      <div className="bg-white text-center rounded-3xl border shadow-lg p-10 max-w-xs">
        <form onSubmit={handleSubmit}>
          <input onChange={(e) => setId(e.target.value)} value={id} type="text" className='p-3 border-solid border-2 border-indigo-600 rounded-md' placeholder="Search by ID" />
          <button className='bg-indigo-600 px-2 mt-5 text-xl rounded text-gray-100'>Search</button>
        </form>

        {err ? (
          <p className='my-5'>No Data Was Found!</p>
        ) : (
          <>
            <img className='my-5 w-50 h-50 rounded-xl shadow-lg mx-auto' alt={`${data}`} src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${data.id}.png`} />
            <h1 className='underline decoration-double decoration-sky-500 text-lg text-gray-700'>{data.name}</h1>
            <h3 className='text-md text-gray-500'>ID:{data.id}</h3>
          </>
        )}
      </div>
      <div className='space-x-4'>
        <span className="text-xl box-decoration-clone bg-gradient-to-r from-indigo-600 to-pink-500 text-white px-4">
          __TodoLists__
        </span>
        <input type="text" placeholder="Task..." name="task" value={task} onChange={handleChange} className='flex flex-col p-3 border-solid border-2 border-indigo-600 rounded-md' />
        <input type="number" placeholder="Deadline (in Days)..." name="deadline" value={deadline} onChange={handleChange} className='flex flex-col p-3 border-solid border-2 border-indigo-600 rounded-md' />
        <br />
        <Button onClick={addTask} variant="contained" className='bg-gradient-to-r from-indigo-600 to-pink-500'>Add Task</Button>
        <div>
          <br />
          {todoList.map((task: ITask, key: number) => {
            return <TodoTask key={key} task={task} completeTask={completeTask} />;
          })}
        </div>
      </div>

    </div>
  );
}

export default App;
